/**
 * Created by huch on 24.01.2018.
 */
export class User {

  username:string;
  password:string;

  constructor(username:string, password:string) {
    this.username = username;
    this.password = password;
  }

}
