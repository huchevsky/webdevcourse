import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CatsComponent} from "./cats/cats.component";
import {HomeComponent} from "./home/home.component";
import {WhiskiesComponent} from "./whiskies/whiskies.component";
import {UsersComponent} from "./users/users.component";
import {ProfileComponent} from "./profile/profile.component";
import {WhiskersComponent} from "./whiskers/whiskers.component";

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'cats', component: CatsComponent },
  { path: 'whiskies', component: WhiskiesComponent },
  { path: 'users', component: UsersComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'whiskers', component: WhiskersComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
