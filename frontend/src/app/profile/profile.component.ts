import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Whisky} from "../whiskies/whisky";
import {Cat} from "../cats/cat";
import { Router } from '@angular/router';

export interface Profile {
  _id:string;
  username:string;
  password:string;
  name:string;
  surname:string;
  __v: number;
}

export interface Whisker {
  _id:string;
  catId:string;
  userId:string;
  whiskyId:string;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profile:Profile;
  whiskers:Whisker[];
  cats:Cat[] = [];
  whiskies:Whisky[] = [];

  constructor(public http:HttpClient, private router: Router) {
  }

  ngOnInit() {
    this.getProfile();
    this.getWhiskers();
  }

  deleteProfile($event) {
    this.http.delete<String>('http://localhost:3000/users/' + localStorage.getItem('session'), {withCredentials:true})
      .subscribe((data: String) => console.log(data));
    localStorage.removeItem("session");
    this.router.navigate(['/users']);
  }

  updateProfile($event) {
    let headers = new HttpHeaders();

    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
      .set("Access-Control-Allow-Origin", "http://localhost:3000")
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      .set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    this.http.put<String>('http://localhost:3000/users/' + localStorage.getItem('session'),
      JSON.stringify({
        name: this.profile.name,
        surname: this.profile.surname,
        password: this.profile.password}),
      {headers:headers, withCredentials:true})
      .subscribe((data: String) => this.getProfile());
  }

  getProfile() {
    return this.http.get<Profile>('http://localhost:3000/users/' + localStorage.getItem('session'), {withCredentials:true})
      .subscribe((data:Profile) => this.profile = data);
  }

  getWhiskers() {
    return this.http.get<Whisker[]>('http://localhost:3000/whiskers', {withCredentials:true})
      .subscribe((data:Whisker[]) => this.setupNames(data));
  }

  setupNames(problem) {
    this.whiskers = problem;
    for (var i = 0; i < problem.length; i++) {
      this.http.get<Cat>('http://localhost:3000/cats/' + problem[i].catId, {withCredentials:true})
        .subscribe((data:Cat) => this.cats.push(data));
      this.http.get<Whisky>('http://localhost:3000/whiskies/' + problem[i].whiskyId, {withCredentials:true})
           .subscribe((data:Whisky) => this.whiskies.push(data));
    }
  }

  deleteWhisker(index) {
    this.http.delete<String>('http://localhost:3000/whiskers/' + this.whiskers[index]._id, {withCredentials:true})
      .subscribe((data: String) => this.getWhiskers());
  }

}
