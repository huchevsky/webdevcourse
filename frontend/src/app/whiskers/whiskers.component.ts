import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import {Cat} from "../cats/cat";
import {Whisky} from "../whiskies/whisky";


@Component({
  selector: 'app-whiskers',
  templateUrl: './whiskers.component.html',
  styleUrls: ['./whiskers.component.css']
})
export class WhiskersComponent implements OnInit {

  cats:Cat[];
  whiskies:Whisky[];
  image:string;
  buttonName:string;
  disabled:boolean;
  outputMessage:string;

  selectedWhisky:Whisky;
  selectedCat:Cat;

  constructor(private router:Router, public http:HttpClient) { }


  disableButton() {
    if (this.disabled == false &&  this.selectedWhisky != null && this.selectedCat != null) {
      return false;
    }
    return true;
  }

  onSelectWhisky(whisky: Whisky): void {
    this.selectedWhisky = whisky;
    console.log(this.selectedWhisky);
  }

  onSelectCat(cat: Cat): void {
    this.selectedCat = cat;
    console.log(this.selectedCat);
  }

  ngOnInit() {
    if(localStorage.getItem('session') == null) {
      this.router.navigate(["/home"]);
    }

    this.getCats();
    this.getWhiskies();
    this.image = null;
    this.disabled = false;
    this.buttonName = 'Combine';
    this.outputMessage = null;
  }

  changeImage() {
    this.image = 'https://media.giphy.com/media/Crdyd3KhccSNa/giphy.gif';
    this.buttonName = null;
    this.disabled = true;
    this.outputMessage = 'Whisker created and added to your Whisker wallet.';
    this.createWhisker();
  }

  getCats() {
    return this.http.get<Cat[]>('http://localhost:3000/cats?name=&fur=&type=')
      .subscribe((data: Cat[]) => this.cats = data);
  }

  getWhiskies() {
    return this.http.get<Whisky[]>('http://localhost:3000/whiskies?name=&region=')
      .subscribe((data: Whisky[]) => this.whiskies = data);
  }

  createWhisker() {
    let headers = new HttpHeaders();

    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
      .set("Access-Control-Allow-Origin", "http://localhost:3000")
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      .set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    this.http.post<String>('http://localhost:3000/whiskers',
      JSON.stringify({
        userId : localStorage.getItem("session"),
        catId: this.selectedCat._id,
        whiskyId: this.selectedWhisky._id}),
      {headers:headers})
      .subscribe((data: String) => this.refresh());
  }

  refresh() {
    this.getCats();
    this.getWhiskies();
    this.image = null;
    this.disabled = false;
    this.buttonName = 'Combine';
    this.outputMessage = null;
  }

}
