import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(public http:HttpClient) { }

  isLoggedIn() {
    if(localStorage.getItem("session") != null) {
      return true;
    } else {
      return false;
    }
  }

  logout() {
    if(localStorage.getItem("session") != null) {
      localStorage.removeItem("session");
      let headers = new HttpHeaders();
      headers = headers.set('Content-Type', 'application/json; charset=utf-8')
        .set("Access-Control-Allow-Origin", "http://localhost:3000")
        .set("Access-Control-Allow-Credentials", "true")
        .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
        .set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
      return this.http.post<string>('http://localhost:3000/users/logout', {headers:headers})
        .subscribe((data: string) => console.log(data));
    }
  }
}
