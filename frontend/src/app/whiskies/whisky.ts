/**
 * Created by huch on 24.01.2018.
 */
export class Whisky {

  distillery:string;
  location:string;
  region:string;
  ownedBy:string;
  _id:string;

  constructor(distillery:string, location:string,
              region:string, ownedBy:string, _id:string) {
    this.distillery = distillery;
    this.location = location;
    this.region = region;
    this.ownedBy = ownedBy;
    this._id = _id;
  }

}
