import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Whisky} from "./whisky";

@Component({
  selector: 'app-whiskies',
  templateUrl: './whiskies.component.html',
  styleUrls: ['./whiskies.component.css']
})
export class WhiskiesComponent implements OnInit {

  whiskies:Whisky[];
  regions:string[];
  name:string = '';
  region:string = '';

  //create
  createDistillery:string;
  createLocation:string;
  createRegion:string;
  createOwnedby:string;

  selectedWhisky:Whisky;

  constructor(public http:HttpClient) { }

  ngOnInit() {
    this.getWhiskies();
    this.getRegion();
  }

  onSelect(whisky: Whisky): void {
    this.selectedWhisky = whisky;
    console.log(this.selectedWhisky);
  }

  deselectWhisky($event):void {
    this.selectedWhisky = null;
  }

  setName($event) {
    this.name = $event;
    this.getWhiskies();
  }

  setRegion($event) {
    this.region = $event;
    this.getWhiskies();
  }

  createWhisky() {
    let headers = new HttpHeaders();

    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
      .set("Access-Control-Allow-Origin", "http://localhost:3000")
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      .set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    this.http.post<String>('http://localhost:3000/whiskies',
      JSON.stringify({
        distillery : this.createDistillery,
        location: this.createLocation,
        ownedBy: this.createOwnedby,
        region: this.createRegion}),
      {headers:headers})
      .subscribe((data: String) => this.getWhiskies());
    this.getWhiskies()
  }

  deleteWhisky($event) {
    let headers = new HttpHeaders();

    this.http.delete<String>('http://localhost:3000/whiskies/' + this.selectedWhisky._id)
      .subscribe((data: String) => this.getWhiskies());
    this.selectedWhisky = null;
    this.getWhiskies()
  }

  updateWhisky($event) {
    let headers = new HttpHeaders();

    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
      .set("Access-Control-Allow-Origin", "http://localhost:3000")
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      .set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    this.http.put<String>('http://localhost:3000/whiskies/' + this.selectedWhisky._id,
      JSON.stringify({
        distillery: this.selectedWhisky.distillery,
        location: this.selectedWhisky.location,
        ownedBy: this.selectedWhisky.ownedBy,
        region: this.selectedWhisky.region}),
      {headers:headers})
      .subscribe((data: String) => this.getWhiskies());
    this.getWhiskies()
  }

  getWhiskies() {
    return this.http.get<Whisky[]>('http://localhost:3000/whiskies?name=' + this.name + "&region=" + this.region)
      .subscribe((data: Whisky[]) => this.whiskies = data);
  }

  getRegion() {
    return this.http.get<string[]>('http://localhost:3000/whiskies/regions')
      .subscribe((data: string[]) => this.regions = data.sort());
  }

}
