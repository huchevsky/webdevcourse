/**
 * Created by huch on 24.01.2018.
 */
import { Component, Input} from '@angular/core';

@Component({
  selector: 'cat-box',
  template: ''
})
export class Cat {

  @Input() breed:string;
  @Input() country:string;
  @Input() origin:string;
  @Input() bodyType:string;
  @Input() coat:string;
  @Input() pattern:string;
  @Input() image:string;
  @Input() _id:string;

  constructor(breed:string,
              bodyType:string,
              coat:string,
              country:string,
              image:string,
              origin:string,
              pattern:string,
              _id:string) {
    this.breed = breed;
    this.country = country;
    this.origin = origin;
    this.bodyType = bodyType;
    this.coat = coat;
    this.pattern = pattern;
    this.image = image;
    this._id = _id;
  }

}
