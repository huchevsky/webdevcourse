import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {Cat} from "./cat";


@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.css'],
})
export class CatsComponent implements OnInit {

  cats:Cat[];
  allFurs:string[];
  allTypes:string[];
  name:string = '';
  fur:string = '';
  type:string = '';

  selectedCat:Cat;

  //create
  createBreed:string;
  createCountry:string;
  createOrigin:string;
  createBodytype:string;
  createCoat:string;
  createPattern:string;
  createImage:string;

  constructor(public http:HttpClient) { }

  ngOnInit() {
    this.getCats();
    this.getFurs();
    this.getTypes();
  }

  onSelect(cat: Cat): void {
    this.selectedCat = cat;
    console.log(this.selectedCat);
  }

  deselectCat($event):void {
    this.selectedCat = null;
  }

  setName($event) {
    this.name = $event;
    this.getCats();
  }

  setFur($event) {
    this.fur = $event;
    this.getCats();
  }

  setType($event) {
    this.type = $event;
    this.getCats();
  }

  createCat() {
    let headers = new HttpHeaders();

    console.log(this.createBreed);
    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
      .set("Access-Control-Allow-Origin", "http://localhost:3000")
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      .set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    this.http.post<String>('http://localhost:3000/cats',
      JSON.stringify({
          breed: this.createBreed,
          country: this.createCountry,
          origin: this.createOrigin,
          bodyType: this.createBodytype,
          coat: this.createCoat,
          pattern: this.createPattern,
          image: this.createImage}),
      {headers:headers})
      .subscribe((data: String) => this.getCats());
    this.getCats();
  }

  deleteCat($event) {
    let headers = new HttpHeaders();

    console.log(this.createBreed);
    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
      .set("Access-Control-Allow-Origin", "http://localhost:3000")
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      .set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    this.http.delete<String>('http://localhost:3000/cats/' + this.selectedCat._id)
      .subscribe((data: String) => this.getCats());
    this.selectedCat = null;
    this.getCats();
  }

  updateCat($event) {
    let headers = new HttpHeaders();

    console.log(this.createBreed);
    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
      .set("Access-Control-Allow-Origin", "http://localhost:3000")
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      .set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    this.http.put<String>('http://localhost:3000/cats/' + this.selectedCat._id,
      JSON.stringify({
        breed: this.selectedCat.breed,
        country: this.selectedCat.country,
        origin: this.selectedCat.origin,
        bodyType: this.selectedCat.bodyType,
        coat: this.selectedCat.coat,
        pattern: this.selectedCat.pattern,
        image: this.selectedCat.image}),
      {headers:headers})
      .subscribe((data: String) => this.getCats());
    this.getCats();
  }

  getCats() {
    let headers = new HttpHeaders();

    //console.log(this.createBreed);
    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
      .set("Access-Control-Allow-Origin", "http://localhost:3000")
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      //.set('Authorization', `Bearer ` + localStorage.getItem('session'))
    return this.http.get<Cat[]>('http://localhost:3000/cats?name=' +
      this.name + "&fur=" + this.fur + "&type=" + this.type, {headers:headers, withCredentials: true })
      .subscribe((data: Cat[]) => this.cats = data);
  }

  getFurs() {
    return this.http.get<string[]>('http://localhost:3000/cats/furs')
      .subscribe((data: string[]) => this.allFurs = data.sort());
  }

  getTypes() {
    return this.http.get<string[]>('http://localhost:3000/cats/types')
      .subscribe((data: string[]) => this.allTypes = data.sort());
  }

}
