import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  username:string;
  password:string;
  reqUsername:string;
  reqPassword:string;
  reqName:string;
  reqSurname:string;
  temp:string;

  failed:boolean;
  failedReg:boolean;
  successfulReg:boolean;

  constructor(private router:Router, public http:HttpClient) { }

  ngOnInit() {
    if(localStorage.getItem('session') != null) {
      this.router.navigate(["/profile"]);
    }
  }

  register() {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      .set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    this.http.post<String>('http://localhost:3000/users/register',
      JSON.stringify({username: this.reqUsername, password: this.reqPassword, name: this.reqName, surname: this.reqSurname}),
      {headers:headers})
      .subscribe((data: String) => this.setup(data),
        err => {
        console.log(err);
          if (err.status == 200) {
            this.failedReg = false;
            this.successfulReg = true;
          } else {
            this.failedReg = true;
            this.successfulReg = false;
          }
        });
  }

  login() {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8')
      .set("Access-Control-Allow-Origin", "http://localhost:3000")
      .set("Access-Control-Allow-Credentials", "true")
      .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
      .set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    this.http.post('http://localhost:3000/users/login',
      JSON.stringify({username: this.username, password: this.password}), {headers:headers, withCredentials:true})
      .subscribe((data: String) => this.setup(data),
        err => {
          this.failed = true;
        });
  }

  setup(data) {
    console.log('test' + data);
    if (localStorage.getItem('session') == null && data != null && data.session != null) {
      localStorage.setItem('session', data.session);
      this.router.navigate(["/profile"]);
    } else {
      console.log("test");
    }
  }

}
