var request = require('request');
var jsdom = require('jsdom');

// Gathers data from Wikipedia page table
exports.getDataFromWikipediaTable = function(handleData, page, startId, endId) {
    var data = [];

    request("https://en.wikipedia.org" + page , function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const { JSDOM } = jsdom;
            const { window } = new JSDOM(body);
            const $ = require('jQuery')(window);

            // Cuts out data from Wikipedia page table
            data = $('table.sortable tr').map(function() {
                return new Array($('td', this).map(function() {
                    return $(this).text()
                }).slice(startId, endId).get())
            }).get();
            handleData(data);
        } else {
            console.log("There was an error: ") + response;
            console.log(body);
        }
    });
}

exports.requiresLogin = function(req, res, next) {
    if (isLoggedIn) {
        return next();
    } else {
        res.status(401).send('You must be logged in to view this page.');
    }
}

exports.isLoggedIn = function(req, res) {
    if (req.session && req.session.userId) {
        return true;
    } else {
        return false;
    }
}

// Logs what happens with database insert action.
exports.onInsert = function(err, entry) {
    if (err) {
        console.log(err)
    } else {
        console.info('Entry was successfully stored - ' + entry);
    }
}
