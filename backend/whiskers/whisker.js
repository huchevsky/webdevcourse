var mongoose = require('mongoose');

var WhiskerScheme = mongoose.Schema({
    userId: {
        type:String,
        required: true
    },
    catId: {
        type:String,
        required: true
    },
    whiskyId: {
        type:String,
        required: true
    }
});

module.exports = mongoose.model('Whisker', WhiskerScheme);
