module.exports = function(app) {

    var wiskers = require('./whisker.controller.js');

    // Create a new Cat
    app.post('/whiskers', wiskers.create);

    // Retrieve all Cats
    app.get('/whiskers', wiskers.findAll);

    // Retrieve a single Cat with catId
    app.get('/whiskers/:whiskersId', wiskers.findOne);

    // Update a Note with noteId
    app.put('/whiskers/:whiskersId', wiskers.update);

    // Delete a Note with noteId
    app.delete('/whiskers/:whiskersId', wiskers.delete);
}