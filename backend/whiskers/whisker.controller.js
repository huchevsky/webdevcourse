var Whisker = require('./whisker.js');

exports.create = function(req, res) {
    console.log('got it');
    if (!req.body.content) {
        var whiskerData = {
            userId: req.body.userId,
            catId: req.body.catId,
            whiskyId: req.body.whiskyId,
        }
        //use schema.create to insert data into the db
        Whisker.create(whiskerData, function (err, whisker) {
            if (err) {
                console.log(err);
                //return next(err);
            } else {
                console.log('created succsssfully');
            }
        });
    }
};

//Return all cats from DB
exports.findAll = function(req, res) {
    console.log(req.session.userId);
    Whisker.find({userId:req.session.userId}).exec(function (err, result) {
        if (err) console.log("Error:", err);
        res.send(result);
    });
};

exports.findOne = function(req, res) {
    // Find a single note with a noteId
};

exports.update = function(req, res) {
    // Update a note identified by the noteId in the request
    // Update a note identified by the noteId in the request
    Whisker.findById(req.params.whiskerId, function(err, whisker) {
        if(err) {
            res.status(500).send({message: "Could not find a note with id " + req.params.whiskerId});
        }

        whisker.whiskyId = req.body.whiskyId;
        whisker.userId = req.body.catId;

        whisky.save(function(err, data){
            if(err) {
                res.status(500).send({message: "Could not update note with id " + req.params.whiskerId});
            } else {
                res.send(data);
            }
        });
    });
};

exports.delete = function(req, res) {
    // Delete a note with the specified noteId in the request
    // Delete a note with the specified noteId in the request
    Whisker.remove({_id: req.params.whiskersId}, function(err, data) {
        if(err) {
            res.status(500).send({message: "Could not delete note with id " + req.params.whiskerId});
        } else {
            res.send({message: "Note deleted successfully!"})
        }
    });
};