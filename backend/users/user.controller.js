var User = require('./user.js');
var UserTools = require('./user.tools.js');
var Tools = require('../tools.js');

// Creates new user ( Register )
exports.create = function(req, res) {
    if(!Tools.isLoggedIn(req)) {
        var userData = {
            username: req.body.username,
            password: req.body.password,
            name: req.body.name,
            surname: req.body.surname,
        }
        User.create(userData, function (err, user) {
            if (err) {
                res.status(400).send("User registration failed.");
            } else {
                res.status(200).send("Registration successful.");
            }
        });
    }
};

// Return all users from DB
exports.findAll = function(req, res) {
    User.find({}).exec(function (err, result) {
        if (err) {
            res.status(400).send("Could not get all users from ");
        }
        res.send(result);
    });
};

// Login into system
exports.login = function(req, res) {
    if(!Tools.isLoggedIn(req)) {
        User.findOne({username: req.body.username, password: req.body.password})
            .exec(function (err, user) {
                if (err) {
                    res.status(400).send("There was an error during login!");
                } else if (!user) {
                    res.status(400).send("User not found in database.");
                } else {
                    req.session.userId = user._id;
                    res.send({session: req.session.userId});
                }
            });
    } else {
        res.status(400).send("Already logged in!");
    }
};

// Logs out user from system
exports.logout = function(req, res) {
    console.log(req.session.userId);
    if(Tools.isLoggedIn(req)) {
        req.session.destroy(function(err) {
            if(err) {
                res.status(400).send("Could not log out user.");
            } else {
                return res.send({response:1});
            }
        });
    } else {
        res.status(400).send("User not logged in.");
    }
};

// Request data about user
exports.findOne = function(req, res) {
    User.findById({_id:req.params.userId}).exec(function (err, result) {
        if (err) {
            res.status(400).send("Can't get information from user in database.");
        }
        res.send(result);
    });
};

// Update a user identified by the userId in the request
exports.update = function(req, res) {
    // Cant update different users
    if (req.params.userId != req.session.userId) {
        res.status(400).send("You cant update different user.");
        return;
    }
    User.findById(req.params.userId, function(err, user) {
        if(err) {
            res.status(400).send("User not found in database.");
        }

        user.name = req.body.name;
        user.surname = req.body.surname;
        user.password = req.body.password;

        user.save(function(err, data){
            if(err) {
                res.status(400).send("Could not update user in database.");
            } else {
                res.send(data);
            }
        });
    });
};

// Delete a user with the specified userId in the request
exports.delete = function(req, res) {
    // Cant delete different users
    if (req.params.userId != req.session.userId) {
        return;
    }

    // Logs out user
    req.session.destroy(function(err) {
        if(err) {
            res.status(400).send("Could not log out user.");
            return;
        }
    });

    User.remove({_id: req.params.userId}, function(err, data) {
        if(err) {
            res.status(400).send("Could not delete user with id - " + req.params.userId);
        } else {
            res.send("User deleted successfully.")
        }
    });
};