module.exports = function(app) {

    var users = require('./user.controller.js');

    // Create a new user
    app.post('/users/register', users.create);

    // Retrieve all users
    app.get('/users', users.findAll);

    // Logs in user into system
    app.post('/users/login', users.login);

    // Logs out user from system
    app.post('/users/logout', users.logout);

    // Retrieve a single user with userId
    app.get('/users/:userId', users.findOne);

    // Update a user with userId
    app.put('/users/:userId', users.update);

    // Delete a user with userId
    app.delete('/users/:userId', users.delete);
}