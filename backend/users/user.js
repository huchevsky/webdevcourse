var mongoose = require('mongoose');

var UserScheme = mongoose.Schema({
    username:{
        type:String,
        required: true,
        unique: true
    },
    password: {
        type:String,
        required: true
    },
    name:String,
    surname:String
});

module.exports = mongoose.model('User', UserScheme);