var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var cors = require('cors');

var dbConfig = require('./database.config.js');

// create express app
var app = express();
mongoose.Promise = global.Promise;

//Set secret for sessions
app.use(session({
    cookie: {
        maxAge: 36000000,
        httpOnly: false // <- set httpOnly to false
    },
    secret: 'whiskeycatpage',
    name: "sessions",
    resave: true,
    saveUninitialized: false,
}));

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse requests of content-type - application/json
app.use(bodyParser.json());

app.options('*', cors({credentials: true, origin: true})); // preflight OPTIONS; put before other routes

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// static folder for images
app.use('/images', express.static('images'));

// Custom Routes
require('./cats/cat.routes.js')(app);
require('./whiskies/whisky.routes.js')(app);
require('./users/user.routes.js')(app);
require('./whiskers/whisker.routes.js')(app);

// Starts application
app.listen(3000, function(){
    console.log("Server is listening on port 3000");
});

// Database connect
mongoose.connect(dbConfig.url, {});

mongoose.connection.on('error', function() {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});
mongoose.connection.once('open', function() {
    console.log("Successfully connected to the database");
});