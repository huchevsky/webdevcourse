var mongoose = require('mongoose');

var CatScheme = mongoose.Schema({
    breed:{
        type:String,
        required: true,
        unique: true
    },
    bodyType:String,
    coat:String,
    country:String,
    image:String,
    origin:String,
    pattern:String
});

module.exports = mongoose.model('Cat', CatScheme);