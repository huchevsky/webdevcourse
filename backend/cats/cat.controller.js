var Cat = require('./cat.js');
var CatTools = require('./cat.tools.js');

// Creates new caat
exports.create = function(req, res) {
    if (req.body.breed) {
        var catData = {
            breed: req.body.breed,
            bodyType: req.body.bodyType,
            coat: req.body.coat,
            country: req.body.country,
            image: req.body.image,
            origin: req.body.origin,
            pattern: req.body.pattern}

        //Tries to insert data into database
        Cat.create(catData, function (err, cat) {
            if (err) {
                res.status(400).send("Cat was not created.");
            } else {
                res.status(200).send("Cat was created successfully.");
            }
        });
    }
};

// Retrieve all cats from DB
exports.findAll = function(req, res) {
    var query = {
        "$and" : [
            {breed : {$regex : '.*' + req.query.name + '.*', '$options' : 'i'}},
            {coat : {$regex : '.*' + req.query.fur + '.*', '$options' : 'i'}},
            {bodyType : {$regex : '.*' + req.query.type + '.*', '$options' : 'i'}}
        ]
    }

    Cat.find(query).exec(function (err, result) {
        if (err) {
            res.status(400).send("Could not get cats from database.");
        }
        res.send(result);
    });
};

// Request data from Wikipedia about cats and put into database
exports.init = function(req, res) {
    CatTools.getCatsFromWikipedia(req, res, Cat);
};

// Retrieve all cat furs from DB
exports.findFurs = function(req, res) {
    Cat.find({}).distinct('coat', function(err, results) {
        if (err) {
            res.status(400).send("Could not gather possible furs from Cat database.");
        }
        res.send(results);
    });
};

// Retrieve all cat body types from DB
exports.findTypes = function(req, res) {
    Cat.find({}).distinct('bodyType', function(err, results) {
        if (err) {
            res.status(400).send("Could not gather body types from Cat database.");
        }
        res.send(results);
    });
};

exports.findOne = function(req, res) {
    // Find a single note with a noteId
    Cat.findById({_id:req.params.catId}).exec(function (err, result) {
        if (err) {
            res.status(400).send("Could not get cat from database.");
        }
        res.send(result);
    });
};

// Update a cat identified by the catId in the request
exports.update = function(req, res) {
    Cat.findById(req.params.catId, function(err, cat) {
        if(err) {
            res.status(400).send("Could not find cat with Id - " + req.params.catId);
        }

        cat.breed = req.body.breed;
        cat.fur = req.body.coat;
        cat.bodyType = req.body.bodyType;
        cat.image = req.body.image;

        cat.save(function(err, data){
            if(err) {
                res.status(400).send("Could not update cat with Id - " + req.params.catId);
            } else {
                res.send(data);
            }
        });
    });
};

// Delete a cat with the specified catId in the request
exports.delete = function(req, res) {
    Cat.remove({_id: req.params.catId}, function(err, results) {
        if(err) {
            res.status(400).send("Could not delete Cat with id - " + req.params.catId);
        } else {
            res.send("Cat deleted successfully with Id - " + req.params.catId);
        }
    });
};