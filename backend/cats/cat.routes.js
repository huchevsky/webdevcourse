var Tools = require('../tools.js');

module.exports = function(app) {

    var cats = require('./cat.controller.js');

    // Create a new cat
    app.post('/cats', cats.create);

    // Retrieve all cat
    app.get('/cats', cats.findAll);

    // Retrieving data from Wikipedia and puts them into db
    app.get('/cats/init', cats.init);

    // Retrieve all cat furs
    app.get('/cats/furs', cats.findFurs);

    // Retrieve all cat types
    app.get('/cats/types', cats.findTypes);

    // Retrieve a single cat with catId
    app.get('/cats/:catId', cats.findOne);

    // Update a cat with catId
    app.put('/cats/:catId', cats.update);

    // Delete a cat with catId
    app.delete('/cats/:catId', cats.delete);
}