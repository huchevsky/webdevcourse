var request = require('request');
var tools = require('../tools.js');

// Gathers data from Wikipedia and puts them into database
exports.getCatsFromWikipedia = function(req, res, Cat) {
    // Put all requested cats into database
    getAllCats(function(output){
        // NOTE: Starts with 1, because that record is empty
        for (i = 1; i < output.length; i++) {

            // Take out data from cat list
            var image = output[i].image;
            if (image == null) image = '';
            var data = {
                breed:output[i][0],
                country:output[i][1],
                origin:output[i][2],
                bodyType:output[i][3],
                coat:output[i][4],
                pattern:output[i][5],
                image:image
            };

            // Inserts cat into database
            Cat.collection.insert(data, tools.onInsert);
        }
    });
}

// Gathers cat from Wikipedia
function getAllCats(handleData) {
    tools.getDataFromWikipediaTable(function(output){
        var calls = 0
        for (i = 1; i < output.length; i++) {
            if (output[i] != -1) {
                // Gathers pictures from Wikipedia api
                getCatPictures(function(output2, cat){
                    if (output2 != null && output2 != '') {
                        cat.image = output2;
                    }
                    calls++;
                    // Deal with data when picture for last cat is done
                    if (calls == output.length - 1) {
                        handleData(output);
                    }
                }, output[i]);
            }
        }
    }, '/wiki/List_of_cat_breeds', 0, 6);
}

// Gathers images for cats requested from Wikipedia
function getCatPictures(handleData, cat) {
    var name = cat[0];
    // Takes string only before ,
    var n = name.indexOf(',');
    name = name.substring(0, n != -1 ? n : name.length);
    // Takes all off if (
    n = name.indexOf('(');
    name = name.substring(0, n != -1 ? n : name.length);
    // Takes all off if [
    n = name.indexOf('[');
    name = name.substring(0, n != -1 ? n : name.length);

    // Checks if it has space in name
    if (name.indexOf(" ") >= 0) {
        name = name.split(' ').join('_');
    } else {
        name = name + "_cat";
    }

    request("https://en.wikipedia.org/w/api.php?action=query&titles=" +
        name + "&prop=pageimages&format=json&pithumbsize=100", function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var json = JSON.parse(body);
            var key = Object.keys(json.query.pages)[0];
            var source = '';

            // Catch exception if there is no source image
            // Else change to multiple ifs
            try {
                source = json.query.pages[key].thumbnail.source;
            } catch(err) {}

            handleData(source, cat);
        } else {
            console.log("There was an error: " + response);
            console.log(body);
        }
    });
}