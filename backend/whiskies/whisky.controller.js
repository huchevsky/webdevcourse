var Whisky = require('./whisky.js');
var WhiskyTools = require('./whisky.tools.js');

// Create new whisky and put into database
exports.create = function(req, res) {
    if (req.body.distillery) {
        var whiskyData = {
            distillery: req.body.distillery,
            location: req.body.location,
            region: req.body.region,
            ownedBy: req.body.ownedBy
        }

        //Tries to insert data into database
        Whisky.create(whiskyData, function (err, whisky) {
            if (err) {
                res.status(400).send("Could not store new Whisky into database.");
            } else {
                res.status(200).send("Whisky created.");
            }
        });
    } else {
        res.status(400).send("Whisky Distillery should be specified.");
    }
};

// Return all whiskies from DB
exports.findAll = function(req, res) {
    var query = {
        "$and" : [
            {distillery : {$regex : '.*' + req.query.name + '.*', '$options' : 'i'}},
            {region : {$regex : '.*' + req.query.region + '.*', '$options' : 'i'}}
        ]
    }
    Whisky.find(query).exec(function (err, result) {
        if (err) {
            res.status(400).send("Whisky not found in database.");
        }
        res.send(result);
    });
};

// Request data from Wikipedia and put into database
exports.init = function(req, res) {
    WhiskyTools.getWhiskiesFromWikipedia(req, res, Whisky);
};

// Return all whisky regions from DB
exports.findRegions = function(req, res) {
    Whisky.find({}).distinct('region', function(err, results) {
        if (err) {
            res.status(400).send("Could not gather Whisky regions from database.");
        }
        res.send(results);
    });
};

// Get one whisky information from database
exports.findOne = function(req, res) {
    Whisky.findById({_id:req.params.whiskyId}).exec(function (err, result) {
        if (err) {
            res.status(400).send("Could not get cat from database.");
        }
        res.send(result);
    });
};

// Update a whisky identified by the whiskyId in the request
exports.update = function(req, res) {
    Whisky.findById(req.params.whiskyId, function(err, whisky) {
        if(err) {
            res.status(400).send("Could not find whisky with whiskyId - " + req.params.whiskyId);
        }

        whisky.distillery = req.body.distillery;
        whisky.location = req.body.location;
        whisky.region = req.body.region;
        whisky.ownedBy = req.body.ownedBy;

        whisky.save(function(err, data){
            if(err) {
                res.status(400).send("Could not update whisky with whiskyId - " + req.params.whiskyId);
            } else {
                res.send(data);
            }
        });
    });
};

// Delete whisky from database with specified whiskyId
exports.delete = function(req, res) {
    Whisky.remove({_id: req.params.whiskyId}, function(err, data) {
        if(err) {
            res.status(400).send("Could not delete whisky with whiskyId - " + req.params.whiskyId);
        } else {
            res.send("Whisky deleted successfully.")
        }
    });
};