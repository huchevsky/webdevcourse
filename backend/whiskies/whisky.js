var mongoose = require('mongoose');

var WhiskyScheme = mongoose.Schema({
    distillery:{
        type:String,
        required: true,
        unique: true
    },
    location:String,
    region:String,
    ownedBy:String
});

module.exports = mongoose.model('Whisky', WhiskyScheme);