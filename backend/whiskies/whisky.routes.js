module.exports = function(app) {

    var whiskies = require('./whisky.controller.js');

    // Create a new whisky
    app.post('/whiskies', whiskies.create);

    // Retrieve all whiskies
    app.get('/whiskies', whiskies.findAll);

    // Retrieving data from Wikipedia and put into database
    app.get('/whiskies/init', whiskies.init);

    // Retrieve all whisky regions
    app.get('/whiskies/regions', whiskies.findRegions);

    // Retrieve a single whisky with whiskyId
    app.get('/whiskies/:whiskyId', whiskies.findOne);

    // Update a whisky with whiskyId
    app.put('/whiskies/:whiskyId', whiskies.update);

    // Delete a whisky with whiskyId
    app.delete('/whiskies/:whiskyId', whiskies.delete);
}