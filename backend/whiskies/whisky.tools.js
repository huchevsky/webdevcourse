var tools = require('../tools.js');

// Gathers data from Wikipedia and puts them into database
exports.getWhiskiesFromWikipedia = function(req, res, Whisky) {
    tools.getDataFromWikipediaTable(function(output){
        for (i = 1; i < output.length; i++) {
            var data = {
                distillery:output[i][0],
                location:output[i][1],
                region:output[i][2],
                ownedBy:output[i][3]
            };

            // Inserts whisky into database
            Whisky.collection.insert(data, tools.onInsert);
        }
    }, '/wiki/List_of_whisky_distilleries_in_Scotland', 0, 4);
}
